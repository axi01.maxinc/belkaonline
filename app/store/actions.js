
function LOG(message) {
  console.log(`#### ACTIONS -> ${message}`)
}

export const SetUserData = ({ commit, state }, value ) => {
  LOG(`SetUserData => value: ${value}`)

  commit('SET_USER_DATA', value);
} 

export const SetUserProfile = ( { commit }, value ) => {
  LOG(`SetUserProfile => value: ${value}`)

  commit('SET_USER_PROFILE', value);
}