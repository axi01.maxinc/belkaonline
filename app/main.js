import Vue from 'nativescript-vue'
import {
  mapGetters,
  mapActions
} from "vuex";

import App from './components/App'

import Navigator from 'nativescript-vue-navigator'
import { routes } from './routes'

import { Auth , Profile} from './utils/api';

import store from './store'
import VueDevtools from 'nativescript-vue-devtools'

if (TNS_ENV !== 'production') {
  Vue.use(VueDevtools)
}

Vue.use(Navigator, { routes })

const Plugin = {
  install(Vue, options) {
    // <top-panel> будет доступен глобально во всем приложении
  //  Vue.component('GTopPanel', TopPanel) 
  //  Vue.component('GBottomPanel', BottomPanel)


    Vue.mixin({
      data() {
        return{
        }
      },

      computed: {
        ...mapGetters({
          token: "GET_TOKEN",

        }),

      },

      methods: {
        ...mapActions([
          "SetUserData",
          "SetUserProfile"
        ]),

        ...Auth,
        ...Profile,
      }
    })
  }
}

Vue.use(Plugin);


// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production')
// Prints Colored logs when --env.production is *NOT* set while building
Vue.config.debug = (TNS_ENV !== 'production')

new Vue({
  store,
  //  render: h => h('frame', [h(App)])
  render: h => h(App)
}).$start()
